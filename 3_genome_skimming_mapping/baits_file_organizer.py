#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import glob
import shutil

#get a list of prefixes for the sampling
files_for_prefix = glob.glob("*OSE_SE_u.fastq.gz")
samples = map(lambda each:each.split("_")[0], files_for_prefix)

markers_files = glob.glob("*.longest")
markers = map(lambda each:each.split(".")[0], markers_files)

sams = glob.glob ("*.sam")
disposable_sams = filter(lambda x: "TRINITY" in x, sams)

fastas = glob.glob("*.fasta")
fastas = filter(lambda x: "_to_" in x, fastas)

##move all the disposable sams to a folder
os.makedirs("disposable_sams")
for f in disposable_sams:
    source = "./" + str(f)
    destination = "./disposable_sams/" + str(f)
    shutil.move(source, destination)

##create a folder for each marker
for f in markers:
    name = str(f) + "_fastas"
    os.makedirs(name)

for marker in markers:
    for sample in samples:
        sam = "./" + str(marker) + "_" + str(sample) + ".sam"
        bam = "./" + str(marker) + "_" + str(sample) + "_sorted.bam"
        bai = "./" + bam + ".bai"
        dest_sam = "./" + str(marker) + "_fastas/" + str(marker) + "_" + str(sample) + ".sam"
        dest_bam = "./" + str(marker) + "_fastas/" + str(marker) + "_" + str(sample) + "_sorted.bam"
        dest_bai = "./" + str(marker) + "_fastas/" + bam + ".bai"
        shutil.move(sam, dest_sam)
        shutil.move(bam, dest_bam)
        shutil.move(bai, dest_bai)
        
for fasta in fastas:
    prefix = fasta.split("_")[0]
    src = "./" + fasta
    dest = "./" + str(prefix) + "_fastas/" + fasta
    shutil.move(src, dest)
        