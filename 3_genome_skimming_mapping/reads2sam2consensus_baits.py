#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import glob
import subprocess

#get a list of prefixes for the sampling
files_for_prefix = glob.glob("*OSE_SE_u.fastq.gz")
samples = map(lambda each:each.split("_")[0], files_for_prefix)

markers_files = glob.glob("*.longest")
markers = map(lambda each:each.split(".")[0], markers_files)

for marker in markers:
    for sample in samples:
        pe1 = str(sample) + "_PE1_u.fastq.gz"
        pe2 = str(sample) + "_PE2_u.fastq.gz"
        se = str(sample) + "_OSE_SE_u.fastq.gz"
        out = str(marker) + "_" + str(sample) + ".sam"
        ref = str(marker) + ".nuc.fna.longest"
        null = "null"
        cmd = ["bbwrap.sh"," in1=",pe1,",",se," in2=",pe2,",",null,\
        " outm=",out," append"," ref=",ref," nodisk -Xmx24g "," bamscript=bs.sh;"," sh bs.sh;"\
        " sam2consensus.py -i ",out]
        cmd = "".join(cmd)
        print cmd
        process = subprocess.Popen(cmd.split(),stdout=subprocess.PIPE)
        output, error = process.communicate()
        
