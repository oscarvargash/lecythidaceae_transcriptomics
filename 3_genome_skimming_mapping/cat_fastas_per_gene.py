#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script to concatenate the fastas in a single file and change
the name of their sequences to their accesion number
then these files are allocated in a single folder named aln
"""
import os, sys
from Bio import SeqIO
import glob
import subprocess
import pandas as pd
import shutil

os.makedirs("aln")
folders  = glob.glob ("*_fastas")
acc = pd.read_csv("accessions_species.csv")
acc["accession"] = acc["accession"].apply(str)
replacements = dict(zip(acc.accession, acc.specie))

for f in folders:
    print "processing folder " + str(f)
    gene = f.split("_")[0]
    os.chdir(("./"+str(f)))             #go into the folders
    fastas = glob.glob("*.fasta")       #collect all the fasta names
    fastas_str = " ".join(fastas)
    out = str(gene) + ".fa"
    os.system("cat "+fastas_str+" > "+out)

    #once a fasta file with all the baited sequences is created
    #a name change is done in each fastas file so names are meaningful
    
    records = list(SeqIO.parse(out,format= "fasta"))
    out2 = str(gene) + ".codes.fa"
    for record in records:
        name = str(record.name)
        sample = name.split("_")[1]
        record.id = sample
        record.name = ""
        record.description = ""
    with open(out2, "w") as output_handle:
        SeqIO.write(records,output_handle,"fasta")
        
    ## now that we only have the code as the name
    ## we will replace this code with code more understandable
    
    out3 = str(gene) + ".names.fa"
    lines = []
    with open(out2) as infile:
        for line in infile:
            for src, target in replacements.iteritems():
                line = line.replace(src, target)
            lines.append(line)
    with open(out3, 'w') as outfile:
        for line in lines:
            outfile.write(line)
    
    ## Finally let's copy all the fastas with the species names as sequences names
    ## into a new directory to carry downstream phylogenetic analysis
    
    source = "./" + out3
    dest = "../aln/" + out3
    shutil.move(source, dest)
    
    os.chdir("..")

