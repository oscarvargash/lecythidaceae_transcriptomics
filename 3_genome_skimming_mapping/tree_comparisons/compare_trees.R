#R script to compare draw two trees face to face 

library(phytools)
library(ape)

dir()

nc <- read.tree("RAxML_bipartitions.concat")
nc <- root(nc, "Barringtonia_edulis", resolve.root=TRUE)
nc <- ladderize(nc, right=T)

cp <- read.tree("RAxML_bipartitions.cp.tre")
cp <- root(cp, "Barringtonia_edulis", resolve.root=TRUE)
cp <- ladderize(cp, right=T)

association <- cbind(sort(nc$tip.label), sort(cp$tip.label))

plot(nc)
plot(cp)

cophyloplot(cp, nc, assoc = association, length.line=4, space=28, gap=3, use.edge.length =F, type = "phylogram", rotate=TRUE)

#now let's compare the raxml tree nc with the astral tree

nc_ast <- read.tree("astral_168.tre")
nc_ast <- root(nc_ast, "Barringtonia_edulis", resolve.root=TRUE)
nc_ast <- ladderize(nc_ast, right=T)
plot(nc_ast)

association2 <- cbind(sort(nc$tip.label), sort(nc_ast$tip.label))

cophyloplot(nc, nc_ast, assoc = association2, length.line=4, space=28, gap=3, use.edge.length =F, type = "phylogram", rotate=TRUE)








# In ended up using cophyloplot and then editing on illustrator, worked fine

trees <- c(nc,cp)

### soluction 1 http://blog.phytools.org/2017/04/showing-multiple-conflicting.html
h<-sapply(trees,function(x) max(nodeHeights(x)))
plotTree(trees[[which(h==max(h))]],direction="leftwards")

xlim<-get("last_plot.phylo",envir=.PlotPhyloEnv)$x.lim

tips<-setNames(1:Ntip(trees[[1]]), 
               untangle(consensus(trees,p=0.5),"read.tree")$tip.label)

for(i in 1:length(trees)){
  plotTree(trees[[i]],color=make.transparent("blue",0.2),
           tips=tips,add=i>1,direction="leftwards",xlim=xlim[2:1],
           mar=c(4.1,1.1,1.1,1.1),nodes="inner",
           ftype=if(i==1) "i" else "off")
  if(i==1) axis(1)
}
#^ this stuff kind of sucks too

### solution 2 http://blog.phytools.org/2017/04/showing-multiple-conflicting.html

rescale.tree<-function(tree,scale){
  tree$edge.length<-tree$edge.length/max(nodeHeights(tree))*scale
  tree
}
rescaled<-lapply(trees,rescale.tree,scale=1)
plotTree(rescaled[[1]],direction="leftwards")

xlim<-get("last_plot.phylo",envir=.PlotPhyloEnv)$x.lim
## set tip order based on a majority rule consensus tree
tips<-setNames(1:Ntip(rescaled[[1]]), 
               untangle(consensus(rescaled,p=0.5),"read.tree")$tip.label)
## plot our trees
for(i in 1:length(rescaled)){
  plotTree(rescaled[[i]],color=make.transparent("blue",0.2),
           tips=tips,add=i>1,direction="leftwards",xlim=xlim[2:1],
           mar=c(4.1,1.1,1.1,1.1),nodes="inner",
           ftype=if(i==1) "i" else "off")
  if(i==1) axis(1)
}

#Solution 5

rescale.tree<-function(tree,scale){
  tree$edge.length<-tree$edge.length/max(nodeHeights(tree))*scale
  tree
}
rescaled<-lapply(trees,rescale.tree,scale=1)
ylim<-c(0,Ntip(rescaled[[1]])+1)
## get depths of the tree
h<-sapply(rescaled,function(x) max(nodeHeights(x)))
## test plot the longest tree to get the maximum x dimension
plotTree(rescaled[[i]],direction="leftwards")

xlim<-get("last_plot.phylo",envir=.PlotPhyloEnv)$x.lim
## set tip order based on a majority rule consensus tree
tips<-setNames(1:Ntip(rescaled[[1]]), 
               untangle(consensus(rescaled,p=0.5),"read.tree")$tip.label)
## set colors
colors<-rainbow(n=length(rescaled))
## plot our trees
for(i in 1:length(rescaled)){
  y.shift<-(i-median(1:length(rescaled)))/length(rescaled)/2
  plotTree(rescaled[[i]],color=make.transparent(colors[i],0.4),
           tips=tips+y.shift,add=i>1,direction="leftwards",xlim=xlim[2:1],
           mar=c(4.1,1.1,1.1,1.1),nodes="inner",
           ftype=if(i==floor(median(1:length(rescaled)))) "i" else "off",
           ylim=ylim)
  if(i==1) axis(1)
}

rescale.tree<-function(tree,scale){
  tree$edge.length<-tree$edge.length/max(nodeHeights(tree))*scale
  tree
}
rescaled<-lapply(trees,rescale.tree,scale=1)
ylim<-c(0,Ntip(trees[[1]])+1)
## get depths of the tree
h<-sapply(rescaled,function(x) max(nodeHeights(x)))
## test plot the longest tree to get the maximum x dimension
plotTree(rescaled[[i]],direction="leftwards")

xlim<-get("last_plot.phylo",envir=.PlotPhyloEnv)$x.lim
## set tip order based on a majority rule consensus tree
tips<-setNames(1:Ntip(rescaled[[1]]), 
               untangle(consensus(rescaled,p=0.5),"read.tree")$tip.label)
## set colors
colors<-rainbow(n=length(rescaled))
## plot our trees
for(i in 1:length(rescaled)){
  y.shift<-(i-median(1:length(rescaled)))/length(rescaled)/2
  plotTree(rescaled[[i]],color=make.transparent(colors[i],0.4),
           tips=tips+y.shift,add=i>1,direction="leftwards",xlim=xlim[2:1],
           mar=c(4.1,1.1,1.1,1.1),
           ftype=if(i==floor(median(1:length(rescaled)))) "i" else "off",
           ylim=ylim)
  if(i==1) axis(1)
}



