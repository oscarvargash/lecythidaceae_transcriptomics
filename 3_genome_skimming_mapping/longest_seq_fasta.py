#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import glob
import argparse
from Bio import SeqIO

####### Arguments and help ###########
parser = argparse.ArgumentParser(description="\
Script to subselect the longest sequence from each fasta in the folder \
from where the script is excuted and write each it in a new fasta file \
written by Oscar Vargas oscarmvargas.com\
")
parser.add_argument("-e", help="file(s) extension, default = fna", default="fna")
parser.add_argument("-o", help="output suffix defaul = longest ", default="longest")
parser.parse_args()
args = parser.parse_args() 

pattern = '*.' + args.e
suffix = "." + args.o

files = glob.glob(pattern)

for file in files:
    print "processing " + file
    outname = file + suffix
    lengths = list()
    records = list(SeqIO.parse(file, "fasta"))
    for record in records:
        name = str(record.name)
        seq = str(record.seq)
        seq_length = len(seq) - seq.count("-")      #calculate the lenght of the seq
        lengths.append(seq_length)                  #append the length to a list
        #print name + " has " + str(seq_length) + " nucleotides"
    longest_index = lengths.index(max(lengths))     #identify the position of the longest sequence
    SeqIO.write (records[longest_index], outname, "fasta")




