# Target sequence capture in the Brazil nut family (Lecythidaceae): marker selection and in silico capture from genome skimming data #

Vargas OM, Heuertz M, Smith SA, Dick CW. 2019. Target sequence capture in the Brazil nut family (Lecythidaceae): marker selection and in silico capture from genome skimming data. Molecular Phylogenetics and Evolution 135: 98–104 [https://doi.org/10.1016/j.ympev.2019.02.020](https://doi.org/10.1016/j.ympev.2019.02.020)

This is the repository for the paper "Target sequence capture in the Brazil nut family (Lecythidaceae): marker selection and in silico capture from genome skimming data"

The repository is organized in three main folders comprising the three main analysis presented in the paper.

Iniside each one of the folders you will find the code used for every component of the pipeline. `.pbs` files contain the command used for every step.

Be aware that the heading of the `.pbs` file, the code above `#  Put your job commands here:` is only the heading to run the command in a cluster; the actual command, is the code below `#  Put your job commands here:`.

The rest of the files are intermediate files necessary to run the scripts and main results.

If you are looking for the script goldfinder you should go here: [https://bitbucket.org/oscarvargash/goldfinder/](https://bitbucket.org/oscarvargash/goldfinder/)

