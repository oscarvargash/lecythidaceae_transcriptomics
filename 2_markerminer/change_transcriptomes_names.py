# Script to change the names of transcriptomes to fit the format of markerminer

import os, fnmatch

wkdir = os.getcwd()
os.listdir(".")
fasta_files = fnmatch.filter(os.listdir(wkdir), "*.fasta")

x = 1
for file in fasta_files:
    sample_name = file.split(".")[0]
    new_name = "DAT" + str(x) + "-" + sample_name + ".fasta"
    os.rename (file, new_name)
    x = x+1
    print sample_name
    print new_name


