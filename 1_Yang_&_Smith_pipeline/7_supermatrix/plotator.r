# r code to plot occupandy graphs
setwd ("/Users/oscar/Box\ Sync/Lecythidaceae_local/RNA_seq/scripts/7_supermatrix")

a <- as.numeric(read.table("ortho_stats_121")[,1])
a <- sort(a, decreasing=TRUE)
pdf(file="taxon_occupancy_121.pdf")
plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
dev.off()

a <- as.numeric(read.table("ortho_stats_MI")[,1])
a <- sort(a, decreasing=TRUE)
pdf(file="taxon_occupancy_MI.pdf")
plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
dev.off()

a <- as.numeric(read.table("ortho_stats_MO")[,1])
a <- sort(a, decreasing=TRUE)
pdf(file="taxon_occupancy_MO.pdf")
plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
dev.off()

a <- as.numeric(read.table("ortho_stats_RT")[,1])
a <- sort(a, decreasing=TRUE)
pdf(file="taxon_occupancy_RT.pdf")
plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
dev.off()
