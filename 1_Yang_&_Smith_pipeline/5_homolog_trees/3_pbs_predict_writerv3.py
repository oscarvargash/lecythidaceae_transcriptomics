#!/usr/bin/python

#program to modify create a .pbs to build homolog trees

import pandas as pd
import os

os.chdir('/Users/oscar/Box Sync/Lecythidaceae_local/RNA_seq/scripts/5_homolog_trees')

acc = pd.read_csv('folder.csv')
acc.head

#make duplicate of trinity_82415.pbs for every accesion, we will later modify the accession number

for index, row in acc.iterrows():
	lines = []
	file = "0_fasta2trees" + row['folder'] + '.pbs'
	with open('0_fasta2trees.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the ID inside every file to match their ID in the 
#file name

for index, row in acc.iterrows():
	lines = []
	file = "0_fasta2trees" + row['folder'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'0.3_I1.4_e5.12t': row['folder']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)





