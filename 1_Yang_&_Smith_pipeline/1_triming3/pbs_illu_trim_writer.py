#!/usr/bin/python

#program to modify create a .pbs TRIMMOMATIC file for each of the samples

import pandas as pd
import os

os.chdir('/Users/oscar/Box Sync/Lecythidaceae_local/RNA_seq/scripts/triming2/')
acc = pd.read_csv('illu_read_trimming.csv')
acc.head
acc['sample'] = acc['sample'].apply(str)
acc.tail

#make duplicate of illu_trim_sampled.pbs for every accesion, we will the name of the reads

for index, row in acc.iterrows():
	lines = []
	file = "illu_trim_" + row['sample'] + '.pbs'
	with open('illu_trim_82415_sampled.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the name of the files for the reads
#file name

for index, row in acc.iterrows():
	lines = []
	file = "illu_trim_" + row['sample'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'Illu_trim_sam': row['sample'], '-1 82415_CGATGTA_S1_L007_R1_001_sampled.fastq.gz -2 82415_CGATGTA_S1_L007_R2_001_sampled.fastq.gz -o filtered/82415': row['reads']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)



