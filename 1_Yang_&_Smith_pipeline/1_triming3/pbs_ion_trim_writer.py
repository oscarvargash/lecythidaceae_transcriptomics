#!/usr/bin/python

#program to modify create a .pbs TRIMMOMATIC file for each of the samples

import pandas as pd
import os

os.chdir('/Users/oscar/Box Sync/Lecythidaceae_local/RNA_seq/scripts/triming2/')
acc = pd.read_csv('ion_read_trimming.csv')
acc.head
acc['sample'] = acc['sample'].apply(str)
acc.tail

#make duplicate of illu_trim_sampled.pbs for every accesion, we will the name of the reads

for index, row in acc.iterrows():
	lines = []
	file = "ion_trim_" + row['sample'] + '.pbs'
	with open('ion_A_002-03_Edec_sampled.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the name of the files for the reads
#file name

for index, row in acc.iterrows():
	lines = []
	file = "ion_trim_" + row['sample'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'ion_trim_sam': row['sample'], 'R_2017_04_20_13_42_02_user_Proton-133-RNASeq_Myriam_Run_A_03_31_34_19042017-IonXpressRNA_002-03_Edec.sampled.fastq.gz -o filtered/A02-30': row['reads']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)



