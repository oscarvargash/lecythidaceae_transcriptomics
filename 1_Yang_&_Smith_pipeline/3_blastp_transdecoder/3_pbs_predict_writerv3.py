#!/usr/bin/python

#program to modify create a .pbs predict assembly file for each of the samples

import pandas as pd
import os

os.chdir('/Users/oscar/Box Sync/Lecythidaceae_local/RNA_seq/scripts/3_blastp_transdecoder')
acc = pd.read_csv('accesion_correspondences_v2.csv')
acc.tail

#make duplicate of trinity_82415.pbs for every accesion, we will later modify the accession number

for index, row in acc.iterrows():
	lines = []
	file = "predict_" + row['Taxon_ID'] + '.pbs'
	with open('predict_Na-impe-L.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the ID inside every file to match their ID in the 
#file name

for index, row in acc.iterrows():
	lines = []
	file = "predict_" + row['Taxon_ID'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'Na-impe-L': row['Taxon_ID']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)




