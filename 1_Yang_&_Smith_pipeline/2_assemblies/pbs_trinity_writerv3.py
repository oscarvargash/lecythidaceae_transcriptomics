#!/usr/bin/python

#program to modify create a .pbs TRINITY assembly file for each of the samples

import pandas as pd
import os

os.chdir('/Users/oscar/Box Sync/Lecythidaceae_local/RNA_seq/scripts/assemblies_september')
acc = pd.read_csv('accesion_correspondences_v2.csv')
acc['Run_number'] = acc['Run_number'].apply(str)
acc.tail

#make duplicate of trinity_82415.pbs for every accesion, we will later modify the accession number

for index, row in acc.iterrows():
	lines = []
	file = "trinity_" + row['Run_number'] + '.pbs'
	with open('trinity_82415_S_v3.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the ID inside every file to match their ID in the 
#file name

for index, row in acc.iterrows():
	lines = []
	file = "trinity_" + row['Run_number'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'Gu-supe-S-1F': row['Taxon_ID'], '82415_S': row['Run_number']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)


#now we are going to create the saples_files necessary for every room
#lets duplicate

#lets slice our original df in two dataframes since illumina runs have two files and iontorrent just one

#lets duplicate the files
acc_illu = acc.head(16)
for index, row in acc_illu.iterrows():
	lines = []
	file = "files_" + row['Run_number'] + '.txt'
	with open('files_muestra_illu.txt') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#lets change its contents to match each sample

for index, row in acc_illu.iterrows():
	lines = []
	file = "files_" + row['Run_number'] + '.txt'
	with open(file) as infile:		
		for line in infile:
			replacements = {'muestra': row['Run_number']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)


### ION TORRENT
#lets with the iontorrent files 
acc_ion = pd.read_csv('accesion_correspondences_ion.csv')


#make duplicate of trinity_82415.pbs for every accesion, we will later modify the accession number

for index, row in acc_ion.iterrows():
	lines = []
	file = "trinity_" + row['Run_number'] + '.pbs'
	with open('trinity_A02-03Sampled.pbs') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the ID inside every file to match their ID in the 
#file name

for index, row in acc_ion.iterrows():
	lines = []
	file = "trinity_" + row['Run_number'] + '.pbs'
	with open(file) as infile:		
		for line in infile:
			replacements = {'Es-coria-1L-sampled': row['Taxon_ID'], 'A02-03S': row['Run_number']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)


#now create the files____.txt files necessary

for index, row in acc_ion.iterrows():
	lines = []
	file = "files_" + row['Run_number'] + '.txt'
	with open('files_A02-03S.txt') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#lets change its contents to match each sample

for index, row in acc_ion.iterrows():
	lines = []
	file = "files_" + row['Run_number'] + '.txt'
	with open(file) as infile:		
		for line in infile:
			replacements = {'A02-03S': row['Run_number']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)
